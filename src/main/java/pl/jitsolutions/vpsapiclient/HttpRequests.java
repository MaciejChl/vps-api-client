package pl.jitsolutions.vpsapiclient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import pl.jitsolutions.vpsapiclient.model.Employee;

import java.util.List;

@Component
public class HttpRequests {

    private static final Logger log = LoggerFactory.getLogger(HttpRequests.class);
    private static final String SERVER_URL = "http://localhost:8080";

    @Autowired
    public RestTemplate restTemplate;

    void getEmployeeObject() {
        Employee employee = restTemplate.getForObject(
                SERVER_URL + "/api/employees/1", Employee.class);
        log.info("===API REST REQUESTS===\n");
        log.info("=== GET " + SERVER_URL + "/api/employees/1 as Employee Object ===");
        log.info(employee.toString());
        log.info("\n======");
    }

    void getEmployeeResponseDetails() {
        HttpHeaders headers = new HttpHeaders();
        //   headers.set("Header", "value");
        HttpEntity entity = new HttpEntity(headers);
        ResponseEntity<String> response = restTemplate.exchange(
                SERVER_URL + "/api/employees/1", HttpMethod.GET, entity, String.class);

        log.info("=== GET " + SERVER_URL + "/api/employees/1 as ResponseEntity ===");
        log.info("Response status: " + response.getStatusCode().toString());
        log.info("Response headers: " + response.getHeaders().toString());
        log.info("Response body: " + response.getBody());
        log.info("\n======");
    }

    void createEmployee() {
        Employee newEmployee = new Employee(1L, 2L, "Brian Green", 42, "Analyst");
        HttpEntity<Employee> requestBody = new HttpEntity<>(newEmployee);
        ResponseEntity<Employee> response
                = restTemplate.postForEntity(SERVER_URL + "/api/employees", requestBody, Employee.class);
        log.info("=== POST " + SERVER_URL + "/api/employees - create new employee ===");
        log.info("Response status: " + response.getStatusCode().toString());
        log.info("Response headers: " + response.getHeaders().toString());
        log.info("Response body: " + response.getBody());
        log.info("\n======");
    }

    void getAllEmployees() {
        ResponseEntity<List<Employee>> response = restTemplate.exchange(
                SERVER_URL + "/api/employees",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Employee>>() {
                }
        );
        List<Employee> employees = response.getBody();
        log.info("=== GET " + SERVER_URL + "/api/employees as List<Employee> ===");
        employees.forEach(employee -> log.info(employee.toString()));
    }

}
