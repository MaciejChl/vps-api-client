package pl.jitsolutions.vpsapiclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class VpsApiClient {

    public static void main(String[] args) {
        SpringApplication.run(VpsApiClient.class, args);
    }

    @Autowired
    HttpRequests httpRequests;

    @Bean
    public CommandLineRunner run() {
        return args -> {
            httpRequests.getEmployeeObject();
            httpRequests.getEmployeeResponseDetails();
            httpRequests.createEmployee();
            httpRequests.getAllEmployees();
        };
    }

}
